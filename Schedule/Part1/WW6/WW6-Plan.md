# WW6

|  时间段  |  内容    |   讲课 / 实践 |  分工  |    备注   |
| :---    |  :----:  |   :----:    |  :----:    |     ---: |
|    0    | [上节课回顾](../WW5/WW5-Plan.md)，介绍本节课内容 |  讲课    |     CZ     |         |

| 展示顺序 |   组名 | 组长 |               | 开始时间 |
| -------- | ---- | ---- | ------------- | -------- |
| 0        |      |      | 开题报告准备/PPT测试  | 9:00     |
| 1        |      |      | [各组自行安排-小组讨论和工作进展](launch.md)  | 11:00    |
| 2        |      |      | 下周安排-下课 | 11:30    |
|          |      |      |               |          |

|时间段  |    内容    |   讲课 / 实践     |    分工    |   备注       |
| :---  |   :----:    |   :----:    |    :----:    |       ---:   |
|   9   | [展望下节课](../WW7/WW7-Plan.md)，总结本节课  |  讲课    |     TAs     |         |

