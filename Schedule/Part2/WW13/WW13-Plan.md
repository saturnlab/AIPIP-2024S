# WW13

Bring Your Laptop to Class. 



| 时间段 |   内容   | 讲课 / 实践 | 分工 |        备注         |
| :----- | :------: | :---------: | :-----: | -------------------------------: |
|    0   | 课程签到 |  logistics  | TAs | 分组签到 |


|  时间段  |  内容    |   讲课 / 实践 |  分工  |    备注   |
| :---    |  :----:  |   :----:    |  :----:    |     ---: |
|    0    | [上节课回顾](../WW12/WW12-Plan.md)，介绍本节课内容 |  讲课    |     CZ     |         |



| **序号** | **时段**    | **主讲教师** | **内容安排**                                    |   备注         |
| -------- | ----------- | ------------ | -------------------- |------------ |
| 0        | 9:03-9:05   | 助教         | 点名 签到            |                                        |
| 1        | 9:05-9:45   | 报告小组             | 适老化、智能路灯和AIGC-调调酱            |                              |
| 2        | 9:50-10:30  | 指导老师-各组         | 各组辅导            |                            |
| 3        | 10:40-11:20 |    各组组长组织      |    自由讨论        |             |
| 4        |             |          |            |             |



|时间段  |  内容    | 讲课 / 实践     |  分工  |   备注       |
| :---  |   :----:    |   :----:    |    :----:    |       ---:   |
|   4   | [展望下节课](../WW14/WW14-Plan.md)，总结本节课   |  讲课    |     TAs     |         |
|   5   | 11:40-12:30 |  AI Copilot小组（组长：大卫明）    |     课后交流   （每周一组）      |        |
