# 人工智能产品创新实践 （AI-PIP）
Artificial Intelligence Product Innovation Practice, AI-PIP 

01510613-91

01510613-92

## 课程信息

**教学安排：**  [教学安排-24春季](Schedule.md)  Teaching Schedule

**课程发展：**  [课程历史](Course-History.md) Course History

## 教学团队

负责老师：
[张烈](https://www.ad.tsinghua.edu.cn/info/1229/15105.htm)、
[林蔚然](https://www.icenter.tsinghua.edu.cn/info/1103/1400.htm)、
[陈震](http://www.icenter.tsinghua.edu.cn/info/1060/1298.htm)

合教老师：

[李超](https://www.bnrist.tsinghua.edu.cn/info/1152/2518.htm)、郭敏、章屹松、马晓东、王浩宇、高英

校外导师：

刘秋江 北京智优沃科技有限公司总裁，清华大学国家级创新创业教育实践基地导师



## 课程助教

谢鑫、胡耀东




